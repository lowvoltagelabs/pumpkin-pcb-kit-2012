EESchema Schematic File Version 2  date Fri 01 Jun 2012 03:28:13 PM PDT
LIBS:tgp_pcb
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:the_great_pumpkin_pcb-cache
EELAYER 25  0
EELAYER END
$Descr User 6000 4000
encoding utf-8
Sheet 1 1
Title "The Great Pumpkin PCB"
Date "1 jun 2012"
Rev "1"
Comp "Low Voltage Labs"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 2000 1150
Wire Wire Line
	2000 1150 2250 1150
Wire Wire Line
	2000 1050 2000 1250
Wire Wire Line
	2000 1850 2000 2050
Wire Wire Line
	3750 1050 3250 1050
Wire Wire Line
	3750 1250 3750 1050
Wire Wire Line
	3750 1650 3750 1850
Wire Wire Line
	3750 2350 3750 2550
$Comp
L GND #PWR2
U 1 1 4FC683A3
P 2000 2050
F 0 "#PWR2" H 2000 2050 30  0001 C CNN
F 1 "GND" H 2000 1980 30  0001 C CNN
	1    2000 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR3
U 1 1 4FC6836C
P 3750 2550
F 0 "#PWR3" H 3750 2550 30  0001 C CNN
F 1 "GND" H 3750 2480 30  0001 C CNN
	1    3750 2550
	1    0    0    -1  
$EndComp
NoConn ~ 3250 1250
$Comp
L SWITCH_INV SW1
U 1 1 4FC6FF3E
P 2750 1150
F 0 "SW1" H 2550 1300 50  0000 C CNN
F 1 "SWITCH_INV" H 2600 1000 50  0000 C CNN
F 2 "SW_SPDT_TH_VERT" H 2750 1150 60  0001 C CNN
	1    2750 1150
	1    0    0    -1  
$EndComp
$Comp
L BATTERY BT1
U 1 1 4FC68554
P 2000 1550
F 0 "BT1" V 2150 1750 50  0000 C CNN
F 1 "BATTERY" H 2000 1360 50  0000 C CNN
F 2 "CR2032V_1" H 2000 1550 60  0001 C CNN
	1    2000 1550
	0    1    1    0   
$EndComp
$Comp
L +BATT #PWR1
U 1 1 4FC683A8
P 2000 1050
F 0 "#PWR1" H 2000 1000 20  0001 C CNN
F 1 "+BATT" H 2000 1150 30  0000 C CNN
	1    2000 1050
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 4FC68357
P 3750 2100
F 0 "R1" V 3830 2100 50  0000 C CNN
F 1 "330" V 3750 2100 50  0000 C CNN
F 2 "R5" H 3750 2100 60  0001 C CNN
	1    3750 2100
	1    0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 4FC68337
P 3750 1450
F 0 "D1" H 3750 1550 50  0000 C CNN
F 1 "LED" H 3750 1350 50  0000 C CNN
F 2 "LED-10MM" H 3750 1450 60  0001 C CNN
	1    3750 1450
	0    1    1    0   
$EndComp
$EndSCHEMATC
